import React from 'react';
import logo from './logo.svg';
import './App.css';
import {List} from "./List";
import {Reminder} from "./Reminder";
import {Color} from "./Color";
import Header from "./Header";
import Footer from "./Footer";

function App()

{
  const elenchi : List[] = [
  new List("ToDo", Color.Red, 1,
    [new Reminder({name: 'Project', color: Color.Blu, description: 'ahaha', done: true})]),
  new List("Spesa", Color.Green, 2,
      [new Reminder({name: 'Detersivi', color: Color.Pink, description: 'sempre peggio'}),
        { name: "Acqua", color: Color.Yellow, done: true}]),
      new List("Workout", Color.Orange, 3,
          [new Reminder({name: 'Abs', color: Color.Blu, description: 'ahaha', done: false})])]

  return <>
      <Header name="Roberta"/>
      <div>
        {elenchi.map((elenco, index) => {
          return <>
               <p> {elenco.color} {elenco.name} (<b>{elenco.reminders.length}</b>)</p>
            </>})}
      </div>
      <Footer/>
</>
}
export default App;
