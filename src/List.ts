import {Color} from './Color';
import {Reminder} from "./Reminder";

export class List{
    name: string;
    color: Color;
    priority: number;
    reminders: Reminder[];

    constructor(name: string,
    color: Color,
    priority: number,
    reminders: Reminder[]) {
        this.name=name;
        this.color=color;
        this.priority=priority;
        this.reminders= reminders;
    }
}