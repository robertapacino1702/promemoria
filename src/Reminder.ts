import {Color} from './Color';

interface IReminder {
    name: string;
    color: Color;
    priority?: number;
    description?: string;
    done?: boolean;

}
export class Reminder implements IReminder{
    name: string;
    color: Color;
    priority?: number;
    description?: string;
    done?: boolean;

    constructor(obj:IReminder) {
        this.name= obj.name;
        this.color= obj.color;
        this.priority= obj.priority;
        this.description= obj.description;
        this.done= obj.done;
    }
}